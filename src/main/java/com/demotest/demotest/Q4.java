package com.demotest.demotest;

import java.util.Arrays;

public class Q4 {
    public static void main(String[] args) {
        int[] nums = {50,1,2,9};
        System.out.println(largestNumber(nums));
    }

    public static String largestNumber(int[] nums) {
        String[] s = new String[nums.length];
        for(int i=0; i<nums.length; i++)  s[i] = String.valueOf(nums[i]);
        Arrays.sort(s, (a, b) -> (b + a).compareTo(a + b));
        return s[0].equals("0") ? "0" : String.join("",s);
    }
}
