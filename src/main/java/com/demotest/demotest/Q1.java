package com.demotest.demotest;

import java.util.ArrayList;
import java.util.List;

public class Q1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);

        //for-loop

        list.add(20);
        list.add(30);
        list.add(40);

        int sum = 0;
        int count = 0;

        for(int i =0 ; i < list.size();i++){
            sum += list.get(i);
        }

        System.out.println("sum : " +  sum);

    //while loop

        while(list.size() > count){
            sum += list.get(count);

            count ++;
        }

        System.out.println("sum : " +  sum);
    }
}
