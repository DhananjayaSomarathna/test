package com.demotest.demotest;

public class Q3 {
    public static int fiboNumbers(int n)
    {
        if(n == 0){
            return 0;
        }
        else if(n == 1){
            return 1;
        }
        else{
            return fiboNumbers(n-2) + fiboNumbers(n-1);
        }
    }
    public static void main (String[] args) {
        int n = 10;
        for(int i = 0; i < n; i++){
            System.out.print(fiboNumbers(i)+ " ");
        }
    }
}
